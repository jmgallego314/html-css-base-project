## Setup

1) Install [Gulp](http://gulpjs.com/) and [NPM](http://nodejs.org).

2) Install dependencies

```
npm install
```

3) Run Gulp

```
gulp
```

4) Testing
By default, the tests runs with the other GULP tasks, but you can run test with the next commands:

```
gulp test-html
```

```
gulp test-css
```

This tests will generate reports in "dist/reports/" folder.
